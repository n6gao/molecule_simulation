use rustacuda::memory::DeviceCopy;

pub const GRID_SIZE: usize = 200;
pub const X_MAX: f64 = 10.0;
pub const DX: f64 = 2.0 * X_MAX / (GRID_SIZE - 1) as f64;
pub const P: usize = 64;
pub const BETA: f64 = 10.0;
pub const HBAR: f64 = 1.0;
pub const TAU: f64 = BETA / (P as f64);
pub const MASS: f64 = 1.0;
pub const OMEGA: f64 = 1.0;
pub const HARMONIC: u32 = 0;
pub const DOUBLE_WELL: u32 = 1;

pub enum PotentialCategory {
    Harmonic,
    DoubleWell,
}

#[repr(transparent)]
pub struct Grid(pub [f64; GRID_SIZE]);

#[repr(transparent)]
pub struct Grid2D(pub [[f64; GRID_SIZE]; GRID_SIZE]);

impl Grid {
    pub fn zeros() -> Grid { Grid([0.0; GRID_SIZE])}
}

impl Grid2D {
    pub fn zeros() -> Grid2D {
        Grid2D([[0.0; GRID_SIZE]; GRID_SIZE])
    }
    pub fn identity() -> Grid2D {
        let mut grid2d = Grid2D([[0.0; GRID_SIZE]; GRID_SIZE]);
        for i in 0..GRID_SIZE {
            grid2d.0[i][i] = 1.0;
        }
        grid2d
    }
}

unsafe impl DeviceCopy for Grid2D {}
unsafe impl DeviceCopy for Grid {}
