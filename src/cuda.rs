use crate::constants::*;
use rustacuda::function::BlockSize;
use rustacuda::launch;
use rustacuda::memory::DeviceBox;
use rustacuda::prelude::*;
use std::error::Error;
use std::ffi::CString;
use rustacuda::context::SharedMemoryConfig::DefaultBankSize;
use std::ops::Mul;
use std::panic::RefUnwindSafe;
use nalgebra::{DMatrix, DVector, SquareMatrix, Matrix6, SymmetricEigen, MatrixN, Vector};
use std::f64::consts;
use std::f64::consts::{E, PI};
use std::cmp::Ordering::Greater;
use std::collections::HashMap;

pub struct EigenPair {
    pub eigenvalue: f64,
    pub eigenvector: Vec<f64>
}

impl EigenPair {
    pub fn new (eigenvalue: f64, eigenvector: Vec<f64>) -> Self {
        EigenPair{
            eigenvalue,
            eigenvector
        }
    }
}

pub struct  PotentialParameter {
    pub potential_category: u32,
    pub mass: f64,
    pub omega: f64,
}

pub struct CudaContext {
    potential_parameter: PotentialParameter,
    module: Module,
    stream: Stream,
    _context: Context,
}

impl CudaContext {
    pub fn init(potential_parameter: PotentialParameter) -> Result<Self, Box<dyn Error>> {
        rustacuda::init(CudaFlags::empty())?;
        println!("Initialize Succeed");
        let device = Device::get_device(0)?;
        let _ctx = Context::create_and_push(ContextFlags::MAP_HOST | ContextFlags::SCHED_AUTO, device);
        let ptx = CString::new(include_str!("kernel/kernel.ptx"))?;
        let module = Module::load_from_string(&ptx);
        let stream = Stream::new(StreamFlags::NON_BLOCKING, None);

        let cuda = CudaContext {
            potential_parameter,
            module: module?,
            stream: stream?,
            _context: _ctx?
        };
        Ok(cuda)
    }

    pub fn populate_grid(&mut self, input: &mut Grid) -> Result<(), Box<dyn Error>> {
        let mut grid_box : DeviceBox<Grid> = DeviceBox::new(&Grid([0.0; GRID_SIZE]))?;
        let module = &self.module;
        let stream = &self.stream;

        // Calling external functions must use unsafe
        unsafe {
            let result = launch!(
            module.populate_grid<<<((GRID_SIZE/32) + 1) as u32, 32, 0, stream>>>(
                grid_box.as_device_ptr(),
                GRID_SIZE,
                X_MAX,
                DX
            ));
            result?
        }

        self.stream.synchronize()?;
        grid_box.copy_to(input)?;

        Ok(())
    }

    pub fn free_particle_density(&mut self, input: &Grid, rho_free: &mut Grid2D) -> Result<(), Box<dyn Error>> {
        let mut input_box = DeviceBox::new(input)?;
        let mut grid2d_box = DeviceBox::new(&Grid2D::zeros())?;
        let module = &self.module;
        let stream = &self.stream;
        let coefficient = self.potential_parameter.mass/(2.0*PI*TAU*HBAR*HBAR);

        unsafe {
            let result = launch!(
            module.free_particle_density<<<((GRID_SIZE) as u32, (GRID_SIZE) as u32), (1,1), 0, stream>>>(
                input_box.as_device_ptr(),
                grid2d_box.as_device_ptr(),
                GRID_SIZE,
                coefficient
            ));
            result?
        }

        self.stream.synchronize()?;
        grid2d_box.copy_to(rho_free)?;

        Ok(())
    }

    pub fn potential_density(&mut self, input: &Grid, rho_potential: &mut Grid, potential: &mut Grid) -> Result<(), Box<dyn Error>> {
        let mut grid_box = DeviceBox::new(input)?;
        let mut rho_potential_box = DeviceBox::new(&Grid([0.0; GRID_SIZE]))?;
        let mut potential_box = DeviceBox::new(&Grid([0.0; GRID_SIZE]))?;

        let module = &self.module;
        let stream = &self.stream;
        let omega = self.potential_parameter.omega;
        let mass = self.potential_parameter.mass;
        let tau: f64 = BETA/(P as f64);

        if self.potential_parameter.potential_category == 0 {
            let harmonic_coefficient = 0.5*mass*omega*omega;
            unsafe {
                let result = launch!(
                module.harmonic_potential<<<(GRID_SIZE) as u32, 1, 0, stream>>>(
                    grid_box.as_device_ptr(),
                    rho_potential_box.as_device_ptr(),
                    potential_box.as_device_ptr(),
                    GRID_SIZE,
                    harmonic_coefficient,
                    tau
                ));
                result?;
            }
        }
        else if self.potential_parameter.potential_category == 1 {
            unsafe {
                let result = launch!(
                module.double_well_potential<<<(GRID_SIZE) as u32, 1, 0, stream>>>(
                    grid_box.as_device_ptr(),
                    rho_potential_box.as_device_ptr(),
                    potential_box.as_device_ptr(),
                    GRID_SIZE,
                    omega,
                    mass
                ));
                result?;
            }
        }

        self.stream.synchronize()?;
        rho_potential_box.copy_to(rho_potential)?;
        potential_box.copy_to(potential)?;

        Ok(())
    }

    pub fn high_temperature_density(&mut self, rho_potential: &Grid, rho_free:&Grid2D, rho_tau: &mut Grid2D) -> Result<(), Box<dyn Error>> {
        let mut rho_potential_box = DeviceBox::new(rho_potential)?;
        let mut rho_free_box = DeviceBox::new(rho_free)?;
        let mut output_box = DeviceBox::new(&Grid2D([[0.0; GRID_SIZE]; GRID_SIZE]))?;

        let module = &self.module;
        let stream = &self.stream;

        unsafe {
            let result = launch!(
                module.high_temperature_density<<<(GRID_SIZE as u32, GRID_SIZE as u32), (1, 1), 0, stream>>>(
                    rho_potential_box.as_device_ptr(),
                    rho_free_box.as_device_ptr(),
                    output_box.as_device_ptr(),
                    GRID_SIZE
                ));
            result?;
        }

        self.stream.synchronize()?;
        output_box.copy_to(rho_tau)?;

        Ok(())
    }

    pub fn density_matrix(&mut self, rho_tau: &mut Grid2D) -> Result<(), Box<dyn Error>>{
        self.matrix_multiplication(rho_tau, DX);
        Ok(())
    }

    pub fn matrix_power(&mut self, input: &Grid2D, power: usize) -> Result<Grid2D, Box<dyn Error>> {

        return if power == 2 {
            self.matrix_dot(input, input)
        } else if power % 2 == 0 {
            let input_2 = self.matrix_power(input, 2)?;
            self.matrix_power(&input_2, power / 2)
        } else if power == 1 {
            self.matrix_dot(input, &Grid2D::identity())
        } else if power == 0 {
            Ok(Grid2D::identity())
        } else {
            let m2 = self.matrix_power(input, power - 1)?;
            self.matrix_dot(input, &m2)
        }
    }

    pub fn matrix_multiplication(&mut self, input: &mut Grid2D, coefficient: f64) -> Result<(), Box<dyn Error>> {
        let mut matrix_box = DeviceBox::new(input)?;

        let module = &self.module;
        let stream = &self.stream;

        unsafe {
            let result = launch!(
                module.matrix_multiplication<<<((GRID_SIZE) as u32, (GRID_SIZE) as u32), (1, 1), 0, stream>>>(
                    matrix_box.as_device_ptr(),
                    GRID_SIZE,
                    coefficient
                ));
            result?;
        }
        self.stream.synchronize()?;
        matrix_box.copy_to(input)?;

        Ok(())
    }

    pub fn matrix_dot(&mut self, m1: & Grid2D, m2:& Grid2D) -> Result<Grid2D, Box<dyn Error>> {
        let mut output_matrix = Grid2D([[0.0; GRID_SIZE]; GRID_SIZE]);
        let mut output_matrix_box = DeviceBox::new(&output_matrix)?;
        let mut m1_box = DeviceBox::new(m1)?;
        let mut m2_box = DeviceBox::new(m2)?;

        let module = &self.module;
        let stream = &self.stream;

        unsafe {
            let result = launch!(
                module.matrix_dot<<<((GRID_SIZE) as u32, (GRID_SIZE) as u32), (1, 1), 0, stream>>>(
                    m1_box.as_device_ptr(),
                    m2_box.as_device_ptr(),
                    output_matrix_box.as_device_ptr(),
                    GRID_SIZE,
                    GRID_SIZE
                ));
            result?;
        }
        self.stream.synchronize()?;
        output_matrix_box.copy_to(&mut output_matrix)?;
        Ok(output_matrix)
    }

    pub fn partition_function(&mut self, rho_tau: &Grid2D, rho_beta: &Grid2D, potential: &Grid) -> Result<(f64, f64, f64), Box<dyn Error>> {
        let mut z = 0.0;
        let mut z_tau = 0.0;
        let mut v_estim  = 0.0;
        for i in 0..GRID_SIZE {
            z += rho_beta.0[i][i]*DX;
            z_tau += rho_tau.0[i][i]*DX;
            v_estim += rho_beta.0[i][i]*DX*potential.0[i];
        }

        Ok((z, z_tau, v_estim))
    }

    pub fn hamiltonian(&mut self, potential: &Grid, h: &mut Grid2D) -> Result<(), Box<dyn Error>>{
        let mut potential_box = DeviceBox::new(potential)?;
        let mut h_box = DeviceBox::new(&Grid2D([[0.0; GRID_SIZE]; GRID_SIZE]))?;

        let module = &self.module;
        let stream = &self.stream;

        let coefficient = HBAR*HBAR / (2.0*self.potential_parameter.mass*DX*DX);

        unsafe {
            let result = launch!(
                module.hamiltonian<<<((GRID_SIZE) as u32, (GRID_SIZE) as u32), (1, 1), 0, stream>>>(
                    potential_box.as_device_ptr(),
                    h_box.as_device_ptr(),
                    GRID_SIZE,
                    coefficient
                ));
            result?;
        }
        self.stream.synchronize()?;
        h_box.copy_to(h)?;

        Ok(())
    }

    pub fn hamiltonian_eigen(&mut self, h: &mut Grid2D, eigenvalue: &mut Grid, eigenvector: &mut Grid2D) -> Result<(), Box<dyn Error>> {
        let mut matrix = DMatrix::identity(GRID_SIZE, GRID_SIZE);
        for i in 0..GRID_SIZE {
            for j in 0..GRID_SIZE {
                matrix[i*GRID_SIZE+j] = h.0[i][j];
            }
        }

        let eigens = SymmetricEigen::new(MatrixN::from(matrix));
        let mut source = vec![];
        for i in 0..GRID_SIZE {
            let mut eigen_vec: Vec<f64> = vec![];
            for j in 0..GRID_SIZE {
                eigen_vec.push(eigens.eigenvectors[j + i*GRID_SIZE]);
            }
            source.push(EigenPair::new(eigens.eigenvalues[i], eigen_vec));
        }
        source.sort_by(|a, b| a.eigenvalue.partial_cmp(&b.eigenvalue).unwrap());

        for i in 0..GRID_SIZE {
            eigenvalue.0[i] = source[i].eigenvalue;

            for j in 0..GRID_SIZE {
                eigenvector.0[i][j] = source[i].eigenvector[j];
            }
        }

        Ok(())
    }

    pub fn sum_over_states(&mut self, eigenvalue: &Grid) -> f64 {
        eigenvalue.0.iter().map(|x| (E).powf(-BETA*x)).sum()
    }

    pub fn average_energy(&mut self, eigenvalue: &Grid) -> f64 {
        eigenvalue.0.iter().map(|x| (E).powf(-BETA*x) * x).sum()
    }

    pub fn average_potential(&mut self, eigenvector: &Grid2D, eigenvalue: &Grid, potential: &Grid) -> Result<(f64), Box<dyn Error>> {
        let mut potential_box = DeviceBox::new(potential)?;
        let mut eigenvector_box = DeviceBox::new(eigenvector)?;
        let mut eigenvalue_box = DeviceBox::new(eigenvalue)?;
        let mut output_box = DeviceBox::new(&Grid::zeros())?;

        let module = &self.module;
        let stream = &self.stream;

        unsafe {
            let result = launch!(
                module.average_potential<<<(GRID_SIZE) as u32, 1, 0, stream>>>(
                    potential_box.as_device_ptr(),
                    eigenvector_box.as_device_ptr(),
                    eigenvalue_box.as_device_ptr(),
                    output_box.as_device_ptr(),
                    GRID_SIZE,
                    BETA
                ));
            result?;
        }
        self.stream.synchronize()?;
        let mut output = Grid([0.0; GRID_SIZE]);
        output_box.copy_to(&mut output)?;
        let mut output_value = 0.0;
        for i in 0..GRID_SIZE {
            output_value += output.0[i];
        }
        Ok(output_value)
    }

    pub fn loop_over_grid(&mut self, eigenvalue: &Grid, eigenvector: &Grid2D, rho_sos: &mut Grid) -> Result<(), Box<dyn Error>> {
        let mut eigenvalue_box = DeviceBox::new(eigenvalue)?;
        let mut eigenvector_box = DeviceBox::new(eigenvector)?;
        let mut rho_sos_box = DeviceBox::new(&Grid([0.0; GRID_SIZE]))?;

        let module = &self.module;
        let stream = &self.stream;

        unsafe {
            let result = launch!(
                module.loop_over_grid<<<(GRID_SIZE) as u32, 1, 0, stream>>>(
                    eigenvalue_box.as_device_ptr(),
                    eigenvector_box.as_device_ptr(),
                    rho_sos_box.as_device_ptr(),
                    GRID_SIZE,
                    BETA
                ));
            result?;
        }
        self.stream.synchronize()?;
        rho_sos_box.copy_to(rho_sos)?;

        Ok(())
    }

    pub fn compute(&mut self) -> Result<(), Box<dyn Error>> {
        let mut grid = Grid([0.0; GRID_SIZE]);
        self.populate_grid(&mut grid)?;

        let mut rho_free = Grid2D([[0.0; GRID_SIZE]; GRID_SIZE]);
        self.free_particle_density(&grid, &mut rho_free)?;

        let mut rho_potential = Grid([0.0; GRID_SIZE]);
        let mut potential = Grid([0.0; GRID_SIZE]);
        self.potential_density(&grid, &mut rho_potential, &mut potential)?;

        let mut rho_tau = Grid2D([[0.0; GRID_SIZE]; GRID_SIZE]);
        self.high_temperature_density(&rho_potential, &rho_free, &mut rho_tau)?;

        let mut rho_beta = self.matrix_power(&rho_tau, P)?;
        self.matrix_multiplication(&mut rho_beta, DX.powf((P-1) as f64))?;

        let values = self.partition_function(&rho_tau, &rho_beta, &potential)?;
        println!("Z(beta={}, tau={}) = {}", BETA, TAU,values.0);
        println!("Z(beta={}, tau={}) = {}", BETA, TAU,values.2/values.0);

        let mut hamiltonian = Grid2D([[0.0; GRID_SIZE]; GRID_SIZE]);
        self.hamiltonian(&potential, &mut hamiltonian)?;

        let mut eigenvalue = Grid([0.0; GRID_SIZE]);
        let mut eigenvector = Grid2D([[0.0; GRID_SIZE]; GRID_SIZE]);
        self.hamiltonian_eigen(&mut hamiltonian,&mut eigenvalue, &mut eigenvector)?;

        let z_sos = self.sum_over_states(&eigenvalue);
        let e_sos = self.average_energy(&eigenvalue);
        let v_sos = self.average_potential(&eigenvector, &eigenvalue, &potential)?;
        println!("z_sos: {}", z_sos);
        println!("Error: {}", ((TAU-z_sos)/TAU).abs());
        println!("e_sos: {}", e_sos/z_sos);
        println!("v_sos: {}", v_sos/z_sos);

        let mut rho_sos = Grid([0.0; GRID_SIZE]);
        self.loop_over_grid(&eigenvalue, &eigenvector, &mut rho_sos)?;

        Ok(())
    }
}
