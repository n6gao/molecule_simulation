/*
 * Author: Ningyuan Gao
 * 2021/04/26
 */

#include <stdio.h>
#include <math.h>

extern "C" __global__ void populate_grid(double* grid, int grid_size, double x_max, double dx) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= grid_size) {
        return ;
    }
    grid[idx] = -x_max + dx*idx;
}

extern "C" __global__ void free_particle_density(double* grid,
                                                 double * rho_free,
                                                 int size,
                                                 double coef) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx * idy >= size * size) {
        return ;
    }
    rho_free[idx + size * idy] = sqrt(coef) * exp(-coef * M_PI * (grid[idy] - grid[idx]) * (grid[idy] - grid[idx]));
}

extern "C" __global__ void harmonic_potential(double * grid,
                                              double * rho_potential,
                                              double * potential,
                                              int size,
                                              double coefficient,
                                              double tau) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= size) {
        return ;
    }
    double value = coefficient * grid[idx] * grid[idx];
    potential[idx] = value;
    rho_potential[idx] = exp(value * (-tau/2));
}

extern "C" __global__ void double_well_potential(double * grid,
                                                 double * rho_potential,
                                                 double * potential,
                                                 int size,
                                                 double a,
                                                 double b,
                                                 double tau) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= size) {
        return ;
    }
    double value = a * pow(grid[idx], 2) + b * pow(grid[idx], 4);
    potential[idx] = value;
    rho_potential[idx] = exp(value * (-tau/2));
}

extern "C" __global__ void high_temperature_density(double * rho_potential,
                                                    double * rho_free,
                                                    double * rho_tau,
                                                    int size) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx * idy >= size * size) {
        return ;
    }
    rho_tau[idx + idy * size] = (rho_potential[idy] * rho_free[idx + idy * size] * rho_potential[idx]);
}

extern "C" __global__ void matrix_dot(double * m1,
                                      double * m2,
                                      double * output, int
                                      size) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx * idy >= size * size) {
        return ;
    }

    double point_value = 0;
    for (int k=0; k<size; k++){
        point_value += (m1[idy * size + k] * m2[idx + size * k]);
    }

    output[idy * size + idx] = point_value;
}

extern "C" __global__ void hamiltonian(double * potential,
                                       double * hamilton,
                                       int size,
                                       double coef) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx * idy >= size * size) {
        return ;
    }

    if (idx == idy) {
        hamilton[idy * size + idx] = coef * pow(-1, idy-idx) * (M_PI * M_PI / 3) + potential[idx];
    } else {
        hamilton[idy * size + idx] = coef * pow(-1, idy-idx) * 2 / (idy - idx) / (idy - idx);
    }
}

extern "C" __global__ void average_potential(double  * potential,
                                             double * eigenvector,
                                             double * eigenvalue,
                                             double * output,
                                             int size,
                                             double beta) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= size) {
        return ;
    }

    double value = 0;
    for (int k=0;k<size;k++) {
        value += potential[k] * (eigenvector[k + size * idx] * eigenvector[k + size * idx]);
    }

    output[idx] = value * exp(-beta * eigenvalue[idx]);
}

extern "C" __global__ void loop_over_grid(double * eigenvalue,
                                          double * eigenvector,
                                          double * rho_sos,
                                          int size,
                                          double beta) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    if (idx >= size) {
        return ;
    }

    double value = 0;
    for (int k=0;k<size;k++) {
        value += (exp(-beta * eigenvalue[k]) * eigenvector[idx * size + k] * eigenvector[idx * size + k]);
    }

    rho_sos[idx] = value;
}

extern "C" __global__ void matrix_multiplication(double * matrix, int size, double coefficient) {
    int idx = threadIdx.x + blockIdx.x * blockDim.x;
    int idy = threadIdx.y + blockIdx.y * blockDim.y;
    if (idx * idy >= size * size) {
        return ;
    }

    matrix[idy * size + idx] *= coefficient;
}
