mod cuda;
mod constants;

use std::env;
use std::error::Error;
use crate::cuda::PotentialParameter;
use crate::constants::*;
use std::process::Command;

const MAX_GCC_VERSION: u32 = 6;

fn main() -> Result<(), Box<dyn Error>> {
    let filename = "src/kernel/kernel.cu";

    // Compile kernel using NVCC
    let out = Command::new("nvcc")
        .args(&["-ccbin", &format!("gcc-{}", MAX_GCC_VERSION)])
        .args(&[filename, "--ptx", "-o", "src/kernel/kernel.ptx"])
        .output()
        .unwrap();

    if !out.status.success() {
        panic!("{}", std::str::from_utf8(&out.stderr).unwrap());
    } else {
        println!("cargo:rerun-if-changed={}", filename);
    }

    let potential_parameters = PotentialParameter  {
        potential_category: HARMONIC,
        mass: MASS,
        omega: OMEGA
    };

    let now = std::time::Instant::now();

    let mut ctx = cuda::CudaContext::init(potential_parameters).unwrap();
    ctx.compute();

    println!("Time elapsed, {} ms", now.elapsed().as_millis());
    Ok(())
}
