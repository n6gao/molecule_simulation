import numpy as np

# V=1/2 mw^2
# V2=1/4 x^2
# V3=-1/2 x^2 + 1/4 x^4
#potential_flag='quartic'
#potential_flag='double_well'
#potential_parameters={'type':'harmonic','mass':mass,'omega':omega}


def V(x1,potential_parameters):
    if potential_parameters['type']=='harmonic':
        mass=potential_parameters['mass']
        omega=potential_parameters['omega']
        value=(1./2.)*(mass*omega**2)*x1**2
    if potential_parameters['type']=='double_well':
        a=potential_parameters['a']
        b=potential_parameters['b']
        value=a*x1**2+b*x1**4
    return value


def kinetic(size,mass,dx):
    T=np.zeros((size,size),float)
    h_bar=1.
    for i in range(size):
        for ip in range(size):
            T1 = h_bar*h_bar / (2.0*mass*dx*dx) * np.power(-1.,i-ip)
            if i==ip:
                T[i,ip] = T1 * (np.pi*np.pi/3.0)
            else:
                T[i,ip] = T1 * 2.0/((i-ip)*(i-ip))
    return T
